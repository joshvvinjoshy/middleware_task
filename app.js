const express = require('express')
const app = express()
const path = require('path')
const port = process.env.PORT || 8000
const logger = require('./middleware_functions/logger.cjs')
app.use(logger)
app.get('/index.html', (req,res)=>{
    // res.setHeader("content-type", "application/json")
    // res.header.set("Content-Type", "application/json");
    // const head = req.headers;
    // console.log(head)
    // console.log(head)
    // console.log(head["content-type"])
    res.send("inside get method of request to index.html")
})
app.post('/data.json', (req,res)=>{
    res.send("inside post method of request to data.json")
})
app.put('/index.html', (req,res)=>{
    res.send("inside put method of request to index.html")
})
app.delete('/data.json', (req,res)=>{
    res.send("inside delete method of request to data.json")
})
app.all('/logs', (req,res) =>{
    res.sendFile('/home/joshvvinjoshy/mb/middleware_task/log_requests.txt')
})
app.listen(port, ()=>{
    console.log(`Server is listening to port ${port}`)
})