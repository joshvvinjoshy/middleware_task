const { appendFile } = require('fs').promises
const log_request = (req,res,next)=>{
    // console.log(`${req.method} ${req.url}`)
    const data = `${req.method} ${req.url}\n`;
    appendFile('./log_requests.txt', data)
        .then(()=>{

        })
        .catch((console.error))
    next()
}
module.exports = log_request